from PIL import Image
import argparse
import json

parser = argparse.ArgumentParser()

parser.add_argument('infile', type=argparse.FileType('r'), help='map file (.json)')
parser.add_argument('-o','--outfile', type=argparse.FileType('wb'), help='map file (.bin)')

args = parser.parse_args()

xmap = json.load(args.infile)
amap = [ 64*x['vflip'] + 32*x['hflip'] + (x['tile']>=256)*8 + x['pal'] for x in xmap]

if args.outfile:
	args.outfile.write(bytearray(amap))