from PIL import Image
import argparse
import json

parser = argparse.ArgumentParser()

parser.add_argument('infile', type=argparse.FileType('r'), help='map file (.json)')
parser.add_argument('-o','--outfile', type=argparse.FileType('wb'), help='map file (.bin)')

group = parser.add_mutually_exclusive_group()
group.add_argument("-u", "--unsigned", action="store_true", help="tile data at $8000 (default)")
group.add_argument("-s", "--signed", action="store_true", help="tile data at $8800")

parser.add_argument("-d", "--delta", action="store_true", help="store delta")

args = parser.parse_args()

xmap = json.load(args.infile)

tmap = [x['tile'] & 255 for x in xmap]

if args.signed:
	tmap = [x ^ 128 for x in tmap]

if args.delta:
	state = 0
	for i in range(len(tmap)):
		state = tmap[i] - state
		tmap[i] = state & 255

if args.outfile:
	args.outfile.write(bytearray(tmap))