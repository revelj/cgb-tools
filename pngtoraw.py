from PIL import Image
import argparse
import os

parser = argparse.ArgumentParser()

parser.add_argument('input_file', help='picture file')
parser.add_argument("-o", "--output-file")
parser.add_argument("--bit-depth", type=int, default=2)
parser.add_argument("-i", "--interleave", action="store_true")

args = parser.parse_args()

# load image file

try:
    im = Image.open(args.input_file)
except:
    print("Could not open image file '%s'" % args.input_file)
    raise SystemExit

w, h = im.size
raster = im.load()

# create an array of tiles

if args.output_file:

    print("Writing tile data ", end=" ")
    print("to '%s' ..." % args.output_file, end=" ")

    f = open(args.output_file, 'wb')

    if args.interleave:

        for y in range(int(h/8)):

            q = []

            if args.bit_depth == 4:

                for yy in range(8):
                    for z in range(0, 2):
                        m = 1 << z
                        b = sum([((raster[x, y*8+yy] & m) >> z) << (7-x)
                                 for x in range(8)])
                        q.append(b)
                for yy in range(8):
                    for z in range(2, 4):
                        m = 1 << z
                        b = sum([((raster[x, y*8+yy] & m) >> z) << (7-x)
                                 for x in range(8)])
                        q.append(b)

            else:
                for yy in range(8):
                    for z in range(args.bit_depth):
                        m = 1 << z
                        b = sum([((raster[x, y*8+yy] & m) >> z) << (7-x)
                                 for x in range(8)])
                        q.append(b)

            f.write(bytearray(q))

    else:

        for z in range(args.bit_depth):

            q = []

            for y in range(int(h/8)):

                for yy in range(8):
                    m = 1 << z
                    b = sum([((raster[x, y*8+yy] & m) >> z) << (7-x)
                            for x in range(8)])
                    q.append(b)

            f.write(bytearray(q))

    f.close()

    print("done")
