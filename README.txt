cgb-tools, v 1.1

Copyright (c) 2015-2023 David Revelj

dalton@tulou.org

These are some tools I made for converting image files to binaries that can be used with Game Boy software. It's released under MIT license, according to LICENSE.txt.

The following tools are included:

cgb_prep.py		rearranges the colors of a picture to prepare it for tile conversion
tileset.py		converts one or more pictures to a set of tiles
tilemap.py		maps a picture onto a tileset, and outputs a json map file
make_amap.py	creates a binary attribute map file from a json map file
make_tmap.py	creates a binary tile map file from a json map file
palette.py		creates a binary palette file from a picture file
spritemap.py	extracts sprites from a picture and saves the data to a new picture, and writes a json file with sprite positions
make_oam.py		creates a binary oam table from a sprite map and a tile map


Example of usage: converting mypic.gif into mypic_tls.bin, mypic_map.bin, mypic_atr.bin, mypic_pal.bin

$ tileset.py mypic.gif -o mypic_tls.gif
$ tilemap.py mypic.gif mypic_tls.gif -o mypic_map.json
$ make_tmap.py mypic_map.json -o mypic_map.bin
$ make_amap.py mypic_map.json -o mypic_atr.bin
$ palette.py mypic.gif -o mypic_pal.bin
$ pngtoraw.py mypic_tls.png -o mypic_tls.bin


I recommend setting up a makefile or some shell scripts to automate the conversion process.

Cheers,
David Revelj
