from PIL import Image
import argparse
import json

parser = argparse.ArgumentParser()

parser.add_argument('sprite_map', type=argparse.FileType('r'), help='map file (.json)')
parser.add_argument('tile_map', type=argparse.FileType('r'), help='map file (.json)')
parser.add_argument('-o','--outfile', type=argparse.FileType('wb'), help='map file (.bin)')

group = parser.add_mutually_exclusive_group()
group.add_argument("-u", "--unsigned", action="store_true", help="tile data at $8000 (default)")
group.add_argument("-s", "--signed", action="store_true", help="tile data at $8800")

parser.add_argument("-d", "--obj16", action="store_true", help="double sized sprites")
parser.add_argument("-b", "--bank1", action="store_true")

parser.add_argument("-p", "--pad", type=int, help="pad file to size")

parser.add_argument("-t", "--transpose", action="store_true", help="transpose oam rows/cols")

parser.add_argument("--priority", action="store_true", help="set priority bit")

args = parser.parse_args()

smap = json.load(args.sprite_map)
xmap = json.load(args.tile_map)

oam = []

for i in range(len(xmap)):

	x = smap['coords'][i]['x']
	y = smap['coords'][i]['y']
	t = xmap[i]['tile']
	p = xmap[i]['pal'] + xmap[i]['hflip']*32 + xmap[i]['vflip']*64

	if args.bank1:
		p = p+8

	if args.priority:
		p = p+128

	if args.obj16:
		t = t*2

	oam.extend([y,x,t,p])

if args.pad and len(oam) < args.pad:
	n = args.pad-len(oam)
	oam.extend([0]*n)

if args.transpose:
	temp = []
	temp.extend([oam[i] for i in range(0, len(oam), 4)])
	temp.extend([oam[i] for i in range(1, len(oam), 4)])
	temp.extend([oam[i] for i in range(2, len(oam), 4)])
	temp.extend([oam[i] for i in range(3, len(oam), 4)])
	oam = temp

if args.outfile:
	args.outfile.write(bytearray(oam))