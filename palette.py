from PIL import Image
import argparse

def expand(x):
	# expand brightness to compensate for cgb compression
	y=float(x)/255.0
	z=(y+y*y)/2
	w=int(255.0*z)
	return w

parser = argparse.ArgumentParser()

parser.add_argument('infile', help='picture file (gif or png)')

parser.add_argument("-o", "--output", dest="output_file")

parser.add_argument("-n", "--num-colors", type=int)

group = parser.add_mutually_exclusive_group()
group.add_argument("-f", "--floor", action="store_true")
group.add_argument("-c", "--ceil", action="store_true")
group.add_argument("-r", "--round", action="store_true")

args = parser.parse_args()

# load image file

try:
	im = Image.open(args.infile)
except:
	print("Could not open image file '%s'" % args.infile)
	raise SystemExit

w,h = im.size
raster = im.load()

if args.num_colors:
	num_colors = args.num_colors
else:
	used_colors = set([raster[x,y] for x in range(w) for y in range(h)])
	num_colors = (max(used_colors) + 3) & -4	# round to next mutliple of four

# read palette

pal = im.resize((num_colors, 1))
pal.putdata(range(num_colors))
pal = list(pal.convert("RGB").getdata())



pal = [(expand(r),expand(g),expand(b)) for r,g,b in pal]

if args.round:
	pal = [(r+4,g+4,b+4) for r,g,b in pal]
elif args.ceil:
	pal = [(r+7,g+7,b+7) for r,g,b in pal]

pal = [(r/8,g/8,b/8) for r,g,b in pal]

if args.output_file:

	pal = [b*1024 + g*32 + r for r,g,b in pal]
	pal = [[x%256,x/256] for x in pal]
	pal = sum(pal, []) # flatten

	print("Writing palette (%d colors)" % (len(pal)/2),)
	print("to '%s' ..." % args.output_file,)

	f = open(args.output_file, 'wb')

	f.write(bytearray(pal))
	f.close()

	print("done")

else:

	for (i,rgb) in enumerate(pal):
		print(('%2d: ' % i) + ('%2d %2d %2d' % rgb))
