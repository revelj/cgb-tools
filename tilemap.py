from PIL import Image
import argparse
import json

def fliplr(m):

	rows = len(m)
	cols = len(m[0])

	return [[m[r][cols-c-1] for c in range(cols)] for r in range(rows)]

def flipud(m):

	rows = len(m)
	cols = len(m[0])

	return [[m[rows-r-1][c] for c in range(cols)] for r in range(rows)]

parser = argparse.ArgumentParser()

parser.add_argument('picture', help='picture file')
parser.add_argument('tileset', help='tileset file')

#parser.add_argument("-hf", "--h-flip", action="store_true", help="use h-flip redundancy check")
#parser.add_argument("-vf", "--v-flip", action="store_true", help="use v-flip redundancy check")

parser.add_argument("--tile-transpose", action="store_true", help="swap rows and columns")
parser.add_argument("--pixel-transpose", action="store_true", help="swap picture x/y")

parser.add_argument("--bit-depth", type=int, default=2)

parser.add_argument("--pal-offset", type=int, default=0)

parser.add_argument("-os", "--offset", type=int, default=0)


parser.add_argument("-th", "--tile-height", type=int, default=8)

parser.add_argument("-o", "--output-file", help='output file')

args = parser.parse_args()

# load image file

try:
	tset = Image.open(args.tileset)
except:
	print("Could not open image file '%s'" % args.tileset)
	raise SystemExit

try:
	pict = Image.open(args.picture)
except:
	print("Could not open image file '%s'" % args.picture)
	raise SystemExit

if args.pixel_transpose:
	pict = pict.transpose(Image.ROTATE_90).transpose(Image.FLIP_TOP_BOTTOM)

pict_w, pict_h = pict.size

tset_width, tset_height = tset.size
tset_raster = tset.load()

tw = tset_width			# tile width
th = args.tile_height	# tile height

if args.tile_transpose:
	
	pict2 = pict.resize((pict_h,pict_w))

	for row in range(pict_h//th):
		for col in range(pict_w//tw):
			z = pict.crop((col*tw,row*th,col*tw+tw,row*th+th))
			pict2.paste(z, (row*tw,col*th,row*tw+tw,col*th+th))

	pict = pict2

num_tset_tiles = int(tset_height/th)

zero_tile = [[0] * tw] * th

# create a tileset array

tset_tiles = [zero_tile]*num_tset_tiles

bitmask = (1 << (args.bit_depth)) - 1
for row in range(int(tset_height/th)):
	tset_tiles[row] = [[tset_raster[x,y] & bitmask for x in range(tw)] for y in range(row*th,row*th+th)]

# create flipped tiles for comparison

flip1 = [fliplr(tile) for tile in tset_tiles]
flip2 = [flipud(tile) for tile in tset_tiles]
flip3 = [flipud(tile) for tile in flip1]

pict_width, pict_height = pict.size
pict_raster = pict.load()

num_pict_rows = int(pict_height/th)
num_pict_cols = int(pict_width/tw)

pict_tiles = []
pal_vector = []

for row in range(num_pict_rows):
	for col in range(num_pict_cols):
		pal_vector.append((pict_raster[col*8,row*th] >> args.bit_depth)+args.pal_offset)
		pict_tiles.append([[pict_raster[x,y] & bitmask for x in range(col*8,col*8+8)] for y in range(row*th,row*th+th)])

map_size = num_pict_cols * num_pict_rows

t0 = {'tile':0, 'pal':0, 'hflip':0, 'vflip':0}
tmap = [t0] * num_pict_cols * num_pict_rows

error = 0

tile_v  = [0]*map_size
pal_v   = [0]*map_size
hflip_v = [0]*map_size
vflip_v = [0]*map_size

for (i, pict_tile) in enumerate(pict_tiles):

	mapped = 0
	hflip = 0
	vflip = 0
	pal = 0

	if not mapped:
		for (j, tset_tile) in enumerate(tset_tiles):
			if pict_tile == tset_tile:
				mapped = 1
				break

	if not mapped:
		for (j, tset_tile) in enumerate(flip1):
			if pict_tile == tset_tile:
				mapped = 1
				hflip = 1
				break

	if not mapped:
		for (j, tset_tile) in enumerate(flip2):
			if pict_tile == tset_tile:
				vflip = 1
				mapped = 1
				break

	if not mapped:
		for (j, tset_tile) in enumerate(flip3):
			if pict_tile == tset_tile:
				hflip = 1
				vflip = 1
				mapped = 1
				break

	if mapped:

		pal = pal_vector[i]
		tmap[i] = {'tile':j+args.offset, 'pal':pal, 'hflip':hflip, 'vflip':vflip}

	else:

		print('ERROR: could not map tile %d' % i)
		error = 1

if error == 0 and args.output_file:

	f = open(args.output_file, 'w')

	json.dump(tmap, f, indent=4)

	f.close()

else:

	print(len(tmap))
