from PIL import Image
import argparse,json

parser = argparse.ArgumentParser()

parser.add_argument('infile', help='picture file')
parser.add_argument("-o", "--output", dest="output_file", help="save to file")
parser.add_argument("-m", "--meta-output", dest="meta_file", help="save meta-data to file")

parser.add_argument("-tw", "--tile-width", type=int, default=8)
parser.add_argument("-th", "--tile-height", type=int, default=8)

args = parser.parse_args()

th = args.tile_height
tw = args.tile_width

# load image file

try:
	im = Image.open(args.infile)
except:
	print("Could not open image file '%s'" % args.infile)
	raise SystemExit

width, height = im.size

coords = []
tiles = []

while 1:

	#transp = 255
	transp = im.info['transparency']

	im2 = im.copy()

	raster = im2.load()

	for y in range(height):
		for x in range(width):

			if raster[x,y] == transp:

				continue

			coords.append( {'x':x, 'y':y} )

			tile = []

			for y2 in range(y, y+th):

				tile.append(8*[0])

				for x2 in range(x,x+8):

					if y2 < height:

						tile[y2-y][x2-x] = raster[x2,y2]
						raster[x2,y2] = transp

					else:

						tile[y2-y][x2-x] = (raster[x,y]/4)*4

			tiles.append(tile)

	try:
		im.seek(im.tell()+1)
	except:
		break

n = len(tiles)

print(n)

im = im.resize((8,th*n))
raster = im.load()

for i in range(n):

	for y in range(th):
		for x in range(8):

			raster[x,th*i+y] = tiles[i][y][x]

if args.output_file:
	im.save(args.output_file)

meta = {'num': n, 'width':tw, 'height':th, 'coords': coords}

if args.meta_file:
	f = open(args.meta_file, 'w')
	json.dump(meta, f, sort_keys = True, indent = 4, ensure_ascii=False)
	f.close()