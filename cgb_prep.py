from PIL import Image
import argparse
import itertools

def remove_subsets(sets):

	return [s1 for s1 in sets if sum([s1.issubset(s2) for s2 in sets]) == 1]

def remove_redundant(sets):

	sets = [frozenset(s) for s in sets]
	sets = list(set(sets))
	sets = [set(s) for s in sets]

	return sets

def remove_impossible(sets):

	return [s for s in sets if len(s) <= 4]

def optimize(sets, d):

	sets = remove_redundant(sets)
	sets = remove_subsets(sets)

	littletons = [s for s in sets if len(s)<4]

	if len(littletons) < 2:
		return sets

	combinations = [littletons[a].union(littletons[b]) for a,b in itertools.combinations(range(len(littletons)),2)]

	combinations = remove_impossible(combinations)
	combinations = remove_redundant(combinations)
	combinations = remove_subsets(combinations)

	if len(combinations) == 0:
		return sets

	new_sets = [optimize(sets+[c], d+1) for c in combinations]

	new_sets = sorted(new_sets, key=lambda sets: len(sets))

	return new_sets[0]

parser = argparse.ArgumentParser()

parser.add_argument('infile', help='picture file')
parser.add_argument("-o", "--output", dest="output_file", help="save to file")

parser.add_argument("-tw", "--tile-width", type=int, default=8)
parser.add_argument("-th", "--tile-height", type=int, default=8)

args = parser.parse_args()

th = args.tile_height
tw = args.tile_width

# load image file

try:
	im = Image.open(args.infile)
except:
	print("Could not open image file '%s'" % args.infile)
	raise SystemExit

width, height = im.size
raster = im.load()

numColors = 256
pal = im.resize((numColors, 1))
pal.putdata(range(numColors))
pal = list(pal.convert("RGB").getdata())

# create an array of tiles (and positions)

tiles = [[[raster[x,y] for x in range(x0,x0+tw)] for y in range(y0,y0+th)] for y0 in range(0,height,th) for x0 in range(0,width,tw)]

xpos = [x0 for y0 in range(0,height,th) for x0 in range(0,width,tw)]
ypos = [y0 for y0 in range(0,height,th) for x0 in range(0,width,tw)]

# create a set of colors for each tile
colors = [set(sum(tile, [])) for tile in tiles]

for (i,c) in enumerate(colors):
	if len(c) > 4:
		print('WARNING: %d colors in tile at %d,%d ' % (len(c),xpos[i],ypos[i]))
		tiles[i] = [[0 for x in range(tw)] for y in range(th)]
		colors[i] = [0]

colors = optimize(colors, 0)

# NOW MAP THE PICTURE BACK ONTO THE PALETTES WE CREATED

stats = [0]*len(colors)

colors = [list(u) for u in colors]

for (i, t) in enumerate(tiles):

	c = set(sum(t, []))

	for (j, u) in enumerate(colors):

		if c.issubset(u):

			stats[j] = stats[j]+1

			tiles[i] = [[j*4+u.index(tiles[i][y][x]) for x in range(tw)] for y in range(th)]

			break;

	for x in range(tw):
		for y in range(th):
			raster[xpos[i]+x,ypos[i]+y] = tiles[i][y][x]

my_pal=[]

for (i,u) in enumerate(colors):

	print('pal %d: colors=%d usage=%d' % (i, len(u), stats[i]))

	for j in range(4):

		if j < len(u):
			(r,g,b) = pal[u[j]]
			my_pal.append(r)
			my_pal.append(g)
			my_pal.append(b)
		else:
			my_pal.append(255)
			my_pal.append(255)
			my_pal.append(255)

if len(colors) > 8:
	print('WARNING: more than 8 palettes not possible!')


im.putpalette(my_pal)

if args.output_file:
	im.save(args.output_file)
