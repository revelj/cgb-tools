from PIL import Image
import argparse

def fliplr(m):

	rows = len(m)
	cols = len(m[0])

	return [[m[r][cols-c-1] for c in range(cols)] for r in range(rows)]

def flipud(m):

	rows = len(m)
	cols = len(m[0])

	return [[m[rows-r-1][c] for c in range(cols)] for r in range(rows)]

parser = argparse.ArgumentParser()

parser.add_argument('infile', help='picture file', nargs='+')

parser.add_argument("--pixel-transpose", action="store_true", help="swap x/y")
parser.add_argument("-t", "--transpose", action="store_true", help="swap rows/columns")
parser.add_argument("-opt", "--optimize", action="store_true", help="remove redundant tiles")

parser.add_argument("-hf", "--h-flip", action="store_true", help="use h-flip redundancy check")
parser.add_argument("-vf", "--v-flip", action="store_true", help="use v-flip redundancy check")

parser.add_argument("-tw", "--tile-width", type=int, default=8)
parser.add_argument("-th", "--tile-height", type=int, default=8)

parser.add_argument("-d", "--bit-depth", type=int, default=2)

parser.add_argument("-z", "--zero", action="store_true", help="enforce empty tile at position 0")

parser.add_argument("-o", "--output", help='output file')

args = parser.parse_args()

tw = args.tile_width
th = args.tile_height

# load image file

tiles = []

for filename in args.infile:

	try:
		im = Image.open(filename)
	except:
		print("Could not open image file '%s'" % filename)
		raise SystemExit

	if args.pixel_transpose:
		im = im.transpose(Image.ROTATE_90).transpose(Image.FLIP_TOP_BOTTOM)

	width, height = im.size

	while 1:

		raster = im.load()

		# create an array of tiles

		print('number of input tiles = %d' % ( (width/tw) * (height/th)  ))


		if args.zero:
			tiles.append([[0 for x in range(tw)] for y in range(th)])

		error = 0

		rc = []

		if args.transpose:
			for row in range(int(height/th)):
				for col in range(int(width/tw)):
					rc.append((row,col))
		else:
			for col in range(int(width/tw)):
				for row in range(int(height/th)):
					rc.append((row,col))

		for (row,col) in rc:

			x0 = col*tw
			x1 = x0+tw

			y0 = row*th
			y1 = y0+th

			tile = [[raster[x,y] for x in range(x0,x1)] for y in range(y0,y1)]

			colors = list(set([c/4 for c in sum(tile,[])]))
			if len(colors) != 1:
				warning = 1
				#print("WARNING: invalid colors in tile at %d,%d" % (x0,y0)()

			tiles.append(tile)

		try:
			im.seek(im.tell()+1)
		except:
			break

if error:
	raise SystemExit

# strip palette information

mask = (1 << args.bit_depth)-1

for i in range(len(tiles)):

	tiles[i] = [[(tiles[i][y][x] & mask) for x in range(tw)] for y in range(th)]

# create flipped tiles for comparison

flip1 = [fliplr(tile) for tile in tiles] if args.h_flip else tiles
flip2 = [flipud(tile) for tile in tiles] if args.v_flip else tiles
flip3 = [flipud(tile) for tile in flip1] if args.h_flip and args.v_flip else tiles

if args.optimize:

	tmap = [next(j for j in range(i+1)
		if tiles[i] == tiles[j]
		or tiles[i] == flip1[j]
		or tiles[i] == flip2[j]
		or tiles[i] == flip3[j])
		for i in range(len(tiles))]

else:

	tmap = range(len(tiles))

unique = sorted(list(set(tmap)))

print('number of output tiles = %d' % len(unique))

if args.output:

	im=im.resize((tw,len(unique)*th))
	raster = im.load()

	for (i,j) in enumerate(unique):

		for x in range(tw):
			for y in range(th):

				raster[x,th*i+y] = tiles[j][y][x]

	im.save(args.output)
